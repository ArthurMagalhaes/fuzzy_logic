import numpy as np
import skfuzzy as fuzz
from skfuzzy import control as ctrl

qualidade = ctrl.Antecedent(np.arange(0, 11), 'qualidade')
servico = ctrl.Antecedent(np.arange(0, 11), 'serviço')
gorjeta = ctrl.Consequent(np.arange(0, 21), 'gorjeta')

qualidade.automf(number=3, names=['ruim', 'boa', 'saborosa'])
servico.automf(number=3, names=['ruim', 'aceitável', 'bom'])


def mf(functions={'trimf': (fuzz.trimf, [0, 0, 10], [0, 10, 20], [10, 20, 20]),
                  # 'sigmf': (fuzz.sigmf, [5, -1], [], []),
                  # 'gaussmf': (fuzz.gaussmf, [], [], []),
                  # 'pimf': (fuzz.pimf, [], [], [], [])
                  }):

    for k, v in functions.items():

        gorjeta['baixa'] = v[0](gorjeta.universe, v[1])
        gorjeta['media'] = v[0](gorjeta.universe, v[2])
        gorjeta['alta'] = v[0](gorjeta.universe, v[3])

        regra1 = ctrl.Rule(qualidade['ruim'] | servico['ruim'], gorjeta['baixa'])
        regra2 = ctrl.Rule(servico['aceitável'], gorjeta['media'])
        regra3 = ctrl.Rule(qualidade['saborosa'] | servico['bom'], gorjeta['alta'])

        sistema_controle = ctrl.ControlSystem([regra1, regra2, regra3])
        sistema = ctrl.ControlSystemSimulation(sistema_controle)

        sistema.input['qualidade'] = 8
        sistema.input['serviço'] = 6
        sistema.compute()

        print(sistema.output['gorjeta'])
        gorjeta.view()


mf()
